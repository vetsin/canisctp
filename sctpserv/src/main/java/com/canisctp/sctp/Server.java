package com.canisctp.sctp;

import org.springframework.beans.factory.annotation.Value;

public class Server extends Thread
{
	@Value("#applicationProperties")
	private String port;
	
	public Server()
	{
		
	}
	
	/**
	 * Called on shutdown @See destroy-method
	 */
	public void shutdown()
	{
		
	}
	
	@Override
	public void run() 
	{
		System.out.println("Thread run");
	}
}
